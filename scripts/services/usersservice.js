'use strict';

/**
 * @ngdoc service
 * @name webappApp.usersService
 * @description
 * # usersService
 * Service in the webappApp.
 */
angular.module('webappApp')
  .factory('usersService', UsersService);

UsersService.$inject=['$resource'];
function UsersService($resource){
	return $resource('/api/users/:id',
			  {id:'@id'},
			  {
				  'update':	{method:'PUT'},
				  'getCoWorkload' : {method:'GET',params:{service:'co_load_report'},isArray:true}
			  }
	  );
}