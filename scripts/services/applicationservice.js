'use strict';

/**
 * @ngdoc service
 * @name webappApp.applicationService
 * @description
 * # applicationService
 * Service in the webappApp.
 */
angular.module('webappApp')
  .factory('applicationService',ApplicationService);

ApplicationService.$inject=['$resource'];
function ApplicationService($resource){
	return $resource(
			  '/api/applications/:id/',
			  {id:'@id'},
			  {
				  'update':	{method:'PUT'},
				  'getOwned' : {method:'GET',params:{restrict:'owned'},isArray:true},
				  'getUnclaimed' : {method:'GET',params:{restrict:'unclaimed'},isArray:true},
				  'getCompleted' : {method:'GET',params:{restrict:'completed'},isArray:true},
				  'claim' : {method:'POST',params:{action:"claim"}}
			  }
			  );
}