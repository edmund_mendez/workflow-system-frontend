'use strict';

/**
 * @ngdoc service
 * @name webappApp.session
 * @description
 * # session
 * Service in the webappApp.
 */
angular.module('webappApp')
  .service('session', function ($rootScope) {
	  var identity={
			  username:null,
			  roles:[]
	  };
	  
	  function start(data){
		identity.username=	data.principal.username;
		identity.roles=[];
		data.principal.authorities.forEach(function(o){
			identity.roles.push(o.authority);
		});
		$rootScope.$broadcast("sessionStarted");
	  };
	  
	  function end(){
		identity.username=null;
		identity.roles=null;
		identity={};
		$rootScope.$broadcast("sessionEnded");
	  }
	  
	  function hasAnyRole(myRoles){
		  var noRoles = myRoles.length;
		  for(var i=0;i<noRoles;i++){
			  if(identity.roles.indexOf(myRoles[i])>0 || myRoles[i]==="*"){
				  return true;
			  }
		  }
		  return false;
	  }
	  
	  function hasAllRoles(myRoles){
		  var noRoles=myRoles.length;
		  var matchCount=0;
		  for(var i=0;i<noRoles;i++){
			  if(identity.roles.indexOf(myRoles[i])>0){
				  matchCount++;
				  if(matchCount==noRoles){
					  return true;
				  }
			  }
		  }
		  return false;
	  }
	  
	  function getUsername(){
		  return identity.username;
	  }
	  return{
		  start:start,
		  end:end,
		  hasAnyRole:hasAnyRole,
		  hasAllRoles:hasAllRoles,	
		  getUsername:getUsername
	  }
  });
