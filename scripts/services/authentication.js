'use strict';

/**
 * @ngdoc service
 * @name webappApp.authentication
 * @description
 * # authentication
 * Service in the webappApp.
 */
angular.module('webappApp')
  .factory('authenticationSvc', AuthenticateSvc);


AuthenticateSvc.$inject=['$rootScope','$http','session'];

function AuthenticateSvc($rootScope,$http,session){
	var auth = function(credentials, callback) {
	    var headers = credentials ? {authorization : "Basic "
	        + btoa(credentials.username + ":" + credentials.password)
	    } : {};

	    $http.get('/app/user', {headers : headers})
	    .success(function(data) {
	      if (data.name) {
	        $rootScope.authenticated = true;
	        session.start(data);
	      } else {
	        $rootScope.authenticated = false;
	      }
	      callback && callback();
	    })
	    .error(function() {
	      $rootScope.authenticated = false;
	      session.end();
	      callback && callback();
	    });
	}
	return {
		authenticate:function(){
			auth();
		},
		login:function(username,password,callback){
			auth({username:username,password:password},callback);
		},
		logout:function(callback){
	  		$http.post('/logout', {}).finally(function() {
	  			$rootScope.authenticated=false;
	  			session.end();
	  			callback && callback();
	  		});
		},
		isAuthenticated:function(){
			return $rootScope.authenticated;
		}
	}
	
}