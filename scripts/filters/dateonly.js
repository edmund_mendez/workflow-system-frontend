'use strict';

/**
 * @ngdoc filter
 * @name webappApp.filter:dateOnly
 * @function
 * @description
 * # dateOnly
 * Filter in the webappApp.
 */
angular.module('webappApp')
  .filter('dateOnly', function () {
    return function (input) {
    	if(!input) return;
      return input.split(" ")[0];
    };
  });
