'use strict';

/**
 * @ngdoc directive
 * @name webappApp.directive:dateTimePicker
 * @description
 * # dateTimePicker
 */
angular.module('webappApp')
  .directive('dateTimePicker', DateTimePicker);

function DateTimePicker($rootScope) {

    return {
        require: '?ngModel',
        restrict: 'AE',
        scope: {
            pick12HourFormat: '@',
            language: '@',
            useCurrent: '@',
            location: '@',
            format:'@'
        },
        link: function (scope, elem, attrs, ctrl) {
            elem.datetimepicker({
                pick12HourFormat: scope.pick12HourFormat,
                language: scope.language,
                useCurrent: scope.useCurrent,
                format:scope.format
            })
            
            ctrl.$parsers.push(function(value){	//ensure default time is appended when dateonly is inserted
            	value += " 12:00:00 AM";
            	return value;
            });
            
            ctrl.$formatters.push(function(value){	//when value is loaded from model for first time, ensure time is chopped off
            	if(!value) return;
            	return value.split(" ")[0];
            });
            //Local event change
            elem.on('blur', function () {	
            	ctrl.$setViewValue(this.value);
            });
        }
    };
}