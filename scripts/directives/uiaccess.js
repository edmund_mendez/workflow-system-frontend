'use strict';

/**
 * @ngdoc directive
 * @name webappApp.directive:uiAccess
 * @description
 * # uiAccess
 */
angular.module('webappApp')
  .directive('uiAccess', ['session','$rootScope',function (session,$rootScope) {
    return {
      restrict: 'A',
      scope:{
    	  uiAccess:'@'
      },

      link: function postLink(scope, element, attrs) {
    	  //scope.uiAccess=attrs.uiAccess;
    	  $rootScope.$on("sessionStarted",function(){
    		  element.css('display',session.hasAnyRole(scope.uiAccess.split(",")) ? 'block' : 'none');
    	  });
        
      }
    };
  }]);
