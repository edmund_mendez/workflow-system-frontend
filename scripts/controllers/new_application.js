'use strict';

/**
 * @ngdoc function
 * @name webappApp.controller:NewApplicationCtrl
 * @description
 * # NewApplicationCtrl
 * Controller of the webappApp
 */
angular.module('webappApp')
  .controller('NewApplicationCtrl', function (applicationService,$location) {
    var newApplication = this;

    newApplication.app={};
    newApplication.submit=Submit;
    newApplication.appTypes=[{name:"Citizenship by Descent", value:"DESCENT"},{name:"Citizenship by Renunciation", value:"RENUNCIATION"}]

    function Submit(){
    	applicationService.save(newApplication.app,function(newApp,getResponseHeaders){
    		$location.url("/application/"+newApp.id);
    	});
    }
  });
