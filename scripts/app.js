'use strict';

/**
 * @ngdoc overview
 * @name webappApp
 * @description
 * # webappApp
 *
 * Main module of the application.
 */
angular
  .module('webappApp', [
    'ngResource',
    'ngRoute',
    'ngFileUpload',
    'ngAnimate',
    'ngSanitize',
    'angularMoment'
  ])
  .constant("DF",{
	  "DATE_ONLY":"DD/MM/YYYY",
	  "DATE_TIME":"DD/MM/YYYY hh:mm:ss a"
  })
  .constant("CONST_ACTIVITIES",{
	  	"ENTER_APP_DETAILS":"Enter Application Details",
	  	"ASSIGN_OFFICER":"Assign Officer",
		"ATTACH_SUPP_DOCS":"Attach Supporting Documents",
		"FIRST_REVIEW":"First Review",
		"PREPARE_CERT":"Prepare Certificate",
		"SECOND_REVIEW":"Second Review",
		"DIRECTOR_ADJ":"Director Adjudication",
		"PREP_DECL_LETTER":"Prepare Decline Letter",
		"PREP_APPR_LETTER":"Prepare Approval Letter",
		"ATTACH_LETTER":"Attach letter",
		"DISPATCH_LETTER":"Dispatch Letter",
		"COMPLETE_APPLICATION":"Complete Application"
  })
  .constant("USER_ROLES",{
	  "ALL":"*",
	  "ADMIN":"ROLE_ADMIN",
	  "DIRECTOR":"ROLE_PICA_DIRECTOR",
	  "MANAGER":"ROLE_PICA_MANAGER",
	  "CASE_OFFICER":"ROLE_PICA_CASE_OFFICER",
	  "DESK_OFFICER":"ROLE_PICA_DESK_OFFICER",
	  "CASHIER":"ROLE_PICA_CASHIER"
  })
  .service('authInterceptor', function($q,$location) {
    var service = this;

    service.responseError = function(response) {
        if (response.status == 401){
        	console.log("interceptor: HTTP.401, switching to /login");
            $location.url("/login");
        }
        return $q.reject(response);
    };
})
.run(function($rootScope, $location, session, USER_ROLES){
	$rootScope.$on('$routeChangeStart', function(event, next) { 
		 if (next.originalPath === "/login" && $rootScope.authenticated) { 
		   event.preventDefault(); 
		  } else if (next.access && next.access.protect && !$rootScope.authenticated) { 
		   //event.preventDefault(); 
		   //$rootScope.$broadcast("event:auth-loginRequired", {});
		   //$location.url("/login");
		  }else if(next.access && !session.hasAnyRole(next.access.roles)) {
		  //}else if(next.access && !AuthSharedService.isAuthorized(next.access.authorizedRoles)) { 
		   event.preventDefault(); 
		   //$rootScope.$broadcast("event:auth-forbidden", {});
		   //$location.url("/");
		   alert('access denied');
		  } 
	});
})
.config(function($routeProvider,$httpProvider,USER_ROLES) {
    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    
    $routeProvider
	  .when('/unclaimed_applications', {
	  	template:'<applist-view applist="$resolve.appData"  menumark="applications" title="Unclaimed Applications"></applist-view>',
	    access:{
	    	protect:true,
	    	roles:[USER_ROLES.CASE_OFFICER,USER_ROLES.DESK_OFFICER,USER_ROLES.MANAGER,USER_ROLES.DIRECTOR,USER_ROLES.ADMIN]
	    },
	  	resolve:{
	      	appData:['applicationService',function(applicationService){
	      		return applicationService.getUnclaimed();
	      	}]
	      }
	    })
	    .when('/owned_applications', {
	    	template:'<applist-view applist="$resolve.appData"  menumark="applications" title="Applications I own and working on"></applist-view>',
	    	access:{
		    	protect:true,
		    	roles:[USER_ROLES.CASE_OFFICER,USER_ROLES.DESK_OFFICER,USER_ROLES.MANAGER,USER_ROLES.DIRECTOR,USER_ROLES.ADMIN]
		    },
	    	resolve:{
	        	appData:['applicationService',function(applicationService){
	        		return applicationService.getOwned();
	        	}]
	        }
	      })
      .when('/completed_applications', {
      	template:'<applist-view applist="$resolve.appData"  menumark="applications" title="Applications I have completed"></applist-view>',
	    access:{
	    	protect:true,
	    	roles:[USER_ROLES.ALL]
	    },
      	resolve:{
          	appData:['applicationService',function(applicationService){
          		return applicationService.getCompleted();
          	}]
          }
        })
      .when('/application/:id', {
    	template:'<app-form app="$resolve.appData"></app-form>',
	    access:{
	    	protect:true,
	    	roles:[USER_ROLES.ALL]
	    },
        resolve:{
        	appData:['applicationService','$route',function(applicationService,$route){
        		return applicationService.get({id:$route.current.params.id});
        	}]
        }
      })
      .when('/new_application', {
        templateUrl: 'views/new_application.html',
	    access:{
	    	protect:true,
	    	roles:[USER_ROLES.DESK_OFFICER,USER_ROLES.ADMIN]
	    },
        controller: 'NewApplicationCtrl',
        controllerAs: 'newApplication'
      })
      .when('/login', {
    	  template:'<login></login>'
      })
      .when('/dashboard', {
        template: '<dashboard></dashboard>',
	    access:{
	    	protect:true,
	    	roles:[USER_ROLES.ALL]
	    },
      })
      .otherwise({
        redirectTo: '/dashboard'
      });

  });