# README #

This is the front end portion of a workflow application port, previously developed on the Lotus Notes/Domino platform. It is AngularJs based, and interfaces with Spring Boot backend: https://bitbucket.org/edmund_mendez/workflow-system-backend.

It's architecture is that of a REST consumer that targets the REST endpoint implemented by Spring MVC controllers.  Through a component architecture, the interface construction have been simplified, with the added benefit of code reuse.  Some of the support libraries used are:

* Boostrap 3
* Angular Moments
* Angular Loanding Bar
* Angular ChartJs

Scaffolded with Yeoman's Angular Generator- grunt for building.

### What is this repository for? ###

* Showcase a AngularJS SPA (using ngRoute) secured with Spring Security

### How do I get set up? ###

* Build directory not included
* Backend configuration also required
* Available as in person demo only

### Contribution guidelines ###

* None at the moment

### Who do I talk to? ###

* edmundm@gmail.com