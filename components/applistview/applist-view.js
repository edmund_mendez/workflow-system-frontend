angular.module('webappApp')
.component('applistView',{
	bindings:{
		applist:'=',
		menumark:'@',
		title:'@'
	},
	controller: AppListView,
	templateUrl:'components/applistview/applist-view.html'
});

AppListView.$inject=['$rootScope','CONST_ACTIVITIES','$location'];
function AppListView($rootScope,CONST_ACTIVITIES,$location){
	this.applications = this.applist;
	this.getStateDesc = GetStateDesc;
	$rootScope.activeViewURL=$location.url();
	$rootScope._activeM=this.menumark;
	
    function GetStateDesc(state){
    	return CONST_ACTIVITIES[state];
    }
}