angular.module("webappApp").component('appActivity',{
	templateUrl:"components/activities/activity.html",
	controller: ActivityController,
	bindings:{
		activity:'<'
	}
});

ActivityController.$inject=['CONST_ACTIVITIES'];
function ActivityController(CONST_ACTIVITIES){
	var ctrl=this;
	ctrl.CONST_ACTIVITIES=CONST_ACTIVITIES;	//translate activity into descriptive names
	
    ctrl.GetStateDesc=function(state){
    	return CONST_ACTIVITIES[state];
    };
    
    ctrl.icon=function(activity){
    	if(!activity.requireApproval){
    		return "glyphicon-play-circle";
    	}else{
    		return (activity.approvalResult=='A' ? "glyphicon-ok-circle" : "glyphicon-remove-circle");
    	}
    		
    }
    
    ctrl.iconColor=function(activity){
    	if(!activity.requireApproval){
    		return "text-primary";
    	}else{
    		return (activity.approvalResult=='A' ? "text-info" : "text-warning");
    	}
    		
    }
}