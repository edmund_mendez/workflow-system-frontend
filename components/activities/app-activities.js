angular.module("webappApp").component('appActivities',{
	templateUrl:"components/activities/app-activities.html",
	controller: ActivitiesController,
	bindings:{
		appId:'<',
		appOwner:'<',
		activities:'<',
		onActivityUpdated:'&',
		onAssignOfficer:'&'
	}
});

ActivitiesController.$inject=['$http','CONST_ACTIVITIES','session'];
function ActivitiesController($http,CONST_ACTIVITIES,session){

	var ctrl=this;
	ctrl.CONST_ACTIVITIES=CONST_ACTIVITIES;	
	ctrl.session = session;
	
    this.doCompleteActivity = function(uActivity){	//TODO: Put in service
    	$http({
    		  method: 'PUT',
    		  url: '/api/activities/'+uActivity.id,
    		  data: uActivity
    	}).then(function successCallback(response) {
    			ctrl.onActivityUpdated({app:response.data});	//pass updated data (application and activities) above to app-form so that state may be updated 
    	}, function errorCallback(response) {
    			throw new Error("Activity completion update failed!")
    			//console.dir(response);
    	});
    }
	
    ctrl.completeActivity = function(activity,res){
		activity.approvalResult= res ? res : null;	//write approval result, based on which button clicked
		ctrl.doCompleteActivity(activity);
	};
	
	ctrl.assignOfficer=function(officer){
		ctrl.onAssignOfficer({officer:officer});	//call function on root ancestor, who will upate model and propogate changes 
	};
    ctrl.justCloseDlg=function(){
    	
    }
}