angular.module("webappApp")
.component('uiMenu',{
	templateUrl:"components/ui/ui-menu.html",
	controller: UiMenu,
	bindings:{
	}
});

UiMenu.$inject=['$rootScope','authenticationSvc'];
function UiMenu($rootScope,authenticationSvc){
	var ctrl=this;
	ctrl._activeM="home";
	authenticationSvc.authenticate();
	
	/** note to self: Code Smell - put into directive**/
	$('[data-toggle="offcanvas"]').click(function(){
        $('#side-menu').toggleClass('hidden-xs');
      });
	
	$('[data-toggle="collapse"]').click(function(){
      $(this).find('.expander').toggleClass('glyphicon-chevron-down');
      $(this).find('.expander').toggleClass('glyphicon-chevron-left');
    });
}