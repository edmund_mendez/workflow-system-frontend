angular.module("webappApp")
.component('uiTopbar',{
	templateUrl:"components/ui/ui-topbar.html",
	controller: UiTopBar,
	bindings:{
	}
});

UiTopBar.$inject=['$rootScope','authenticationSvc','$location','session'];
function UiTopBar($rootScope,authenticationSvc,$location,session){
	var ctrl=this;
	ctrl.session=session;
	this.Logout=function(){
		authenticationSvc.logout(function(){
			$location.path("/login");
		})
	};		
}