angular.module('webappApp')
.component('appHeader',{
	bindings:{
		app:'<'
	},
	controller: AppHeader,
	templateUrl:'components/appforms/app-header.html'
});
AppHeader.$inject=['applicationService','DF'];
function AppHeader(applicationService,DF){
    var ctrl = this;
    ctrl.DF = DF;	//date formatter
    ctrl.sources=[{"id":1,"sourceName":"Walk-in"},{"id":2,"sourceName":"Mail-in"},{"id":3,"sourceName":"Online"}];
    ctrl.countries=[{id:1,"name":"Jamaica"},{id:2,"name":"USA"},{id:3,"name":"UK"},{id:4,"name":"Canada"},{id:5,"name":"Other"}]
}