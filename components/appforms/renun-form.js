angular.module('webappApp')
.component('renunForm',{
	bindings:{
		app:'<'
	},	
	require:{
		dlgController:'^modalDlg'
	},
	controller: RenunForm,
	templateUrl:'components/appforms/_renunApp.html'
});

RenunForm.$inject=['applicationService','DF'];
function RenunForm(applicationService,DF){
    var ctrl = this;
    ctrl.DF = DF;	//date format
    this.$onInit=function(){	//necessary to call after init so that parent controller is available
    	ctrl.dlgController.initChildComponent(this);	//necessary to pass component reference to wrapping dialog box
    };
    
	/*CONTRACT FUNCTION
	 * Required by all components wrapped by dialog box
	 * Dialog box will call this function when it's 'OK' button is clicked
	 * This function therefore must return all relevant data
	 * NOTE: Validation could be done in this function ahead of return
	 * */
    this.getData=function(){	//called by wrapping dialog box to get out data
    	return ctrl.app;
    };
}