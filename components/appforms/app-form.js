angular.module('webappApp')
.component('appForm',{
	bindings:{
		app:'='
	},
	controller: AppForm,
	templateUrl:'components/appforms/app-form.html'
});

AppForm.$inject=['applicationService','$rootScope','$http','session','DF'];
function AppForm(applicationService,$rootScope,$http,session,DF){
    var ctrl = this;
    ctrl.DF = DF;	//date format
    /**Controller Functions**/
    ctrl.claim=Claim;
    ctrl.save=Save;
    ctrl.activityUpdated = ActivityUpdated;
    ctrl.assignOfficer = AssignOfficer;
    ctrl.updateAttachments=UpdateAttachments;
    ctrl.session=session;	//make service available to view
    
    //Return to the last view marked in rootScope- 
    //if none (like if user refreshed browser while viewing document), go to applications I own
    ctrl.backUrl=$rootScope.activeViewURL ? $rootScope.activeViewURL : "/owned_applications";
    
    function UpdateAttachments(atts){
    	ctrl.app.attachments=atts;
    }
    
    function AssignOfficer(officer){	//Assign case officer
    	$http({
				url:'/api/applications/'+ctrl.app.id,
				method:'PATCH',
				params:{field:'caseofficer'},
				data:officer
    		 }).then(
    			function(response){	//success
    				//console.dir(response.data);
    			 ctrl.app.caseOfficer=response.data.caseOfficer;
    			},
    			function(response){	//error
    				alert("assignment failed!");
    				throw new Error("Case office assignment failed");
        			console.dir(response);
    			});
    }
    
    function ActivityUpdated(app){
    	//consider updating just app.activities and app.currentOwner since nothing else really chagnes
    	ctrl.app = app;
    }
    function Save(){
    	/*applicationService.update(ctrl.app,function(upApp){
    		ctrl.app=upApp;
    	});*/
    	ctrl.app.$update(function(upApp){
    		ctrl.app=upApp;
    	});
    }
    
    function Claim(){
    	if(!session.hasAnyRole(ctrl.app.claimRoles.split(","))){
    		alert("Action denied! Insufficient priviledges");
    		return;
    	}
    	    	
    	ctrl.app.$claim(function(upApp){
    		ctrl.app.currentOwner=upApp.currentOwner;
    	},function(resp){
    		resp.status==403 ? alert("Action forbidden") : alert("General error");
    	});
    }
}