angular.module('webappApp')
.component('appSummary',{
	bindings:{
		app:'<'
	},
	controller: AppSummary,
	templateUrl:'components/appforms/app-summary.html'
});


AppSummary.$inject=['$sce','$interval','DF','moment'];
function AppSummary($sce,$interval,DF,moment){
    var ctrl = this;
    
    ctrl.DF = DF;	//date format
    
    ctrl.photo="images/person-holder.jpg";
    
    ctrl.getFullName=function(){
    	return ctrl.app.middleName ? ctrl.app.firstName +" "+ ctrl.app.middleName +" "+ ctrl.app.lastName : ctrl.app.firstName +" "+ ctrl.app.lastName
    }
    ctrl.getEmail=function(){
    	return ctrl.app.email ? $sce.trustAsHtml(" | <a href = 'mail-to: "+ ctrl.app.email +"'>"+ctrl.app.email+"</a>") : "";
    }
    ctrl.getCaseOfficer=function(){
    	return ctrl.app.caseOfficer ? ctrl.app.caseOfficer.firstName +" "+ ctrl.app.caseOfficer.lastName : "None assigned yet";
    }
    
    var toCancel=$interval(function(){
    	if(ctrl.app.attachments){
    		if(ctrl.app.attachments.length>0){
    			ctrl.app.attachments.forEach(function(att){
    				if(att.fileType=="1"){
    	    			ctrl.photo = "/api/applications/"+ctrl.app.id+"/attachments/"+att.id;
    	    			
    	    			$interval.cancel(toCancel);
    		    		toCancel=undefined;
    	    			return;
    	    		}
    	    	});
	    		$interval.cancel(toCancel);
	    		toCancel=undefined;
    		}
    	}else{
    		console.log('skipped once, because not ready');
    	}
    },100);

   /* getPhoto();
    function getPhoto(){
    	ctrl.app.attachments.forEach(function(att){
    		if(att.fileType=="1"){
    			ctrl.photo = "/api/applications/"+ctrl.app.id+"/attachments/"+att.id;
    			return;
    		}
    	});
    }*/
}