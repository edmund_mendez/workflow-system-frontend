angular.module('webappApp')
.component('assignOfficer',{
	controller: AsnOfficer,
	bindings:{
		state:'@'
	},
	require:{
		dlgController:'^modalDlg'
	},
	templateUrl:"components/activity-support/assign-officer.html"
});

AsnOfficer.$inject=['usersService'];
function AsnOfficer(usersService){
    var ctrl = this;
    
    ctrl.selectedOfficer="";
    
    usersService.getCoWorkload(function(data){
    	//console.log("need to fix this ghost call in assign-officer!");
    	ctrl.report = data;
    });
    
    this.$onInit=function(){	//necessary to call after init so that parent controller is available
    	ctrl.dlgController.initChildComponent(this);	//necessary to pass component reference to wrapping dialog box
    };
    
	/*CONTRACT FUNCTION
	 * Required by all components wrapped by dialog box
	 * Dialog box will call this function when it's 'OK' button is clicked
	 * This function therefore must return all relevant data
	 * NOTE: Validation could be done in this function ahead of return
	 * */
    this.getData=function(){	//called by wrapping dialog box to get out data
    	return ctrl.selectedOfficer;
    };
}