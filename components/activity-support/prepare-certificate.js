angular.module('webappApp')
.component('prepareCertificate',{
	controller: PrepCert,
	bindings:{
		appId:'<'
	},
	require:{
		dlgController:'^modalDlg'
	},
	templateUrl:"components/activity-support/prepare-certificate.html"
});

PrepCert.$inject=['usersService'];
function PrepCert(usersService){
	var ctrl=this;
	ctrl.tplId={
				 id:"",
				 fileType:'CERTIFICATE'
				};
	
	ctrl.tOptions=[{id:'1',templateName:'Basic Descent Template'}];
	
	ctrl.getSaveUrl=function(){
		return "/api/applications/"+ctrl.appId+"/attachments"
	}
}