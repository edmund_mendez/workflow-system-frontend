angular.module('webappApp')
.component('attachSupport',{
	bindings:{
		appId:'<'
	},
	require:{
		appCtrl:'^appForm'
	},
	controller: AttachSupp,
	templateUrl:'components/activity-support/attach-support.html'
});

function AttachSupp(){
	var ctrl=this;
	ctrl.data={filetype:""};
	ctrl.attTypes=[
	              {"id":1,"clsName":"Photograph"},
	              {"id":2,"clsName":"Marriage Certificate"},
	              {"id":3,"clsName":"Birth Certificate"},
	              {"id":3,"clsName":"Death Certificate"}
	              ];
	
	ctrl.typeSelector=function(){
		return ctrl.data.filetype=='1' ? '.jpg,.jpeg,.png,.bmp' : '.pdf,.doc,.docx';
	}
	ctrl.getSaveUrl=function(){
		return "/api/applications/"+ctrl.appId+"/attachments";
	}
	
	ctrl.updateAtt=function(data){
		ctrl.appCtrl.updateAttachments(data);
	}
}