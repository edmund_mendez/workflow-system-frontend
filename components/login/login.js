angular.module('webappApp')
.component('login',{
	templateUrl:'components/login/login.html',
	bindings:{},
	controller:Login
});

Login.$inject=['authenticationSvc', '$location', '$rootScope'];
function Login(authenticationSvc,$location,$rootScope){
	this.credentials = {};
	this.credentials.username="emendez";
	this.credentials.password="password";
	
	this.login = Login;
	
	function Login(){
		authenticationSvc.login(this.credentials.username,this.credentials.password,function() {
		    if ($rootScope.authenticated) {
		      $location.path("/");
		      this.error = false;
		    } else {
		      $location.path("/login");
		      this.error = true;
		    }
		  });
	}
	
}

