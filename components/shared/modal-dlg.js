angular.module('webappApp')
.component('modalDlg',{
		bindings:{
			modalHeight:'@mHeight',
			modalWidth:'@mWidth',
			modalTitle:'@mTitle',
			modalId:'@mId',
			onApply:'&',
			applyLabel:'@'
		},
		controller:ModalDlg,
		transclude:true,
		templateUrl:"components/shared/modal-dlg.html"
});

function ModalDlg(){
	var ctrl=this;
	ctrl.CHILD_CONTROLLER;
	ctrl.buttonLabel = ctrl.applyLabel ? ctrl.applyLabel : "Apply";
	
	ctrl.apply=function(){
		if(!ctrl.CHILD_CONTROLLER){
			throw new Error("Transcluded child component in dialog box not initialized!");
			return;
		}
		ctrl.onApply({data:ctrl.CHILD_CONTROLLER.getData()});
	}
	
	ctrl.initChildComponent=function(controller){	//this function is to be called by component in ng-transclude using inter-component communcation
		ctrl.CHILD_CONTROLLER=controller;
	}
}
