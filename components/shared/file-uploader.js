angular.module('webappApp')
.component('fileUploader',{
	bindings:{
		formData:'<',
		saveUrl:'<',
		typeSelector:'<',
		onUpload:'&'
	},
	controller: FileUploader,
	templateUrl:'components/shared/file-uploader.html'
});

FileUploader.$inject=['Upload','$timeout'];

function FileUploader(Upload,$timeout){
	var ctrl=this;
	ctrl.file={};
	ctrl.progPercentage=0;
	
	ctrl.xfering=0;	//upload status: 0, no upload in progress, 1: upload in progress
	ctrl.uploadMessage="";
	
	var finishUpload=function(resp){
		$timeout(function(){
			ctrl.xfering=0;
			ctrl.uploadMessage="File: '"+ctrl.file.name+"' uploaded successfuly!";
			ctrl.file={};
		},1000);
		
		ctrl.onUpload({data:resp.data})
	}
	
	ctrl.rmSelection=function(){
		ctrl.file=null;
	};
	
	ctrl.submit = function() {
	      if (ctrl.file) {
	        ctrl.upload(ctrl.file);
	      }
	};
	function resetIndicators(){
		ctrl.progPercentage=0;
		ctrl.xfering=1;
		ctrl.uploadMessage="";
	}
	
	ctrl.upload = function (file) {
		resetIndicators();
		var payload = angular.merge({file: file},ctrl.formData);
        Upload.upload({
            url: ctrl.saveUrl,
            method:'PUT',
            data: payload,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
        }).then(function (resp) {
        	finishUpload(resp)
            //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
        }, function (resp) {
        	ctrl.xfering=0;
        	//console.dir(resp);
        	ctrl.uploadMessage='Upload failed. Error status: ' + resp.status;
        }, function (evt) {
        		ctrl.progPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };
    
    
}